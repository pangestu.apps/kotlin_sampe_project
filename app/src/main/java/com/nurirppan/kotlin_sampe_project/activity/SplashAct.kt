package com.nurirppan.kotlin_sampe_project.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import java.util.*
import kotlin.concurrent.schedule
import com.nurirppan.kotlin_sampe_project.R
import kotlinx.android.synthetic.main.activity_splash.*

class SplashAct : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Timer("SettingUp", false).schedule(1000) {
            startActivity(Intent(this@SplashAct, MainActivity::class.java))
            finish()
        }
    }
}
