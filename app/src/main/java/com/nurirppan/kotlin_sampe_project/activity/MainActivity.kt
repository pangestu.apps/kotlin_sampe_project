package com.nurirppan.kotlin_sampe_project.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.nurirppan.kotlin_sampe_project.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setActionOnClick()
    }

    private fun setActionOnClick() {
        btnLogin.setOnClickListener(this)
        btnRegister.setOnClickListener(this)
        btnRecyclerView.setOnClickListener(this)
        btnViewPager.setOnClickListener(this)
        btnGetApi.setOnClickListener(this)
        btnPostApi.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnLogin -> {
                startActivity(Intent(this@MainActivity, LoginAct::class.java))
            }
            R.id.btnRegister -> {
                startActivity(Intent(this@MainActivity, RegisterAct::class.java))
            }
            R.id.btnRecyclerView -> {
                startActivity(Intent(this@MainActivity, RecyclerViewAct::class.java))
            }
            R.id.btnViewPager -> {
                startActivity(Intent(this@MainActivity, ViewPagerAct::class.java))
            }
            R.id.btnGetApi -> {
                Toast.makeText(this, "btnGetApi", Toast.LENGTH_SHORT).show()
            }
            R.id.btnPostApi -> {
                Toast.makeText(this, "btnPostApi", Toast.LENGTH_SHORT).show()
            }
        }
    }

}
