package com.nurirppan.kotlin_sampe_project.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.nurirppan.kotlin_sampe_project.R
import kotlinx.android.synthetic.main.activity_view_pager.*

class ViewPagerAct : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_pager)
    }
}
