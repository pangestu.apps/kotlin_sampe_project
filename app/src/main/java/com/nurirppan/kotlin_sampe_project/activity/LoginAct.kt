package com.nurirppan.kotlin_sampe_project.activity

import android.annotation.SuppressLint
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import com.nurirppan.kotlin_sampe_project.R
import kotlinx.android.synthetic.main.activity_login.*


class LoginAct : AppCompatActivity(), View.OnClickListener {

    private var statusField: Boolean = false
    private var statPhoneNumber: Boolean = false
    private var statFullname: Boolean = false
    private var statPassword: Boolean = false
    private var statRePassword: Boolean = false
    private var statCodeReferal: Boolean = false
    private var statAgreeTermsConditions: Boolean = false
    private var statAgreeConnectedPede: Boolean = false
    private var statAllValidation: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setActionOnClick()
        setActionFieldTextWatcher()
        isEnableButton()
    }

    private fun setActionFieldTextWatcher() {
        etPhoneNumber.addTextChangedListener(TextWatcherCustom(etPhoneNumber, implementAfterChange))
        etFullname.addTextChangedListener(TextWatcherCustom(etFullname, implementAfterChange))
        etEmail.addTextChangedListener(TextWatcherCustom(etEmail, implementAfterChange))
        etPassword.addTextChangedListener(TextWatcherCustom(etPassword, implementAfterChange))
        etRePassword.addTextChangedListener(TextWatcherCustom(etRePassword, implementAfterChange))
        etCodeReferal.addTextChangedListener(TextWatcherCustom(etCodeReferal, implementAfterChange))
    }

    private fun setActionOnClick() {
        btnLogin.setOnClickListener(this)
        setCheckBox()
    }

    private fun setCheckBox() {
        cbAgreeTermsConditions.setOnCheckedChangeListener { compoundButton, isChecked ->
            statAgreeTermsConditions = isChecked
            isEnableButton()
        }
        cbAgreeConnectedPede.setOnCheckedChangeListener { compoundButton, isChecked ->
            statAgreeConnectedPede = isChecked
            isEnableButton()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnLogin -> {
                Toast.makeText(this, "Anda Berhasil Registrasi", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val implementAfterChange = object : TextWatcherCustom.AfterTextChange {
        override fun afterTextChanged(s: Editable, v: View) {
            when (v.id) {
                R.id.etPhoneNumber -> {
                    statPhoneNumber = validationBackgroundView(etPhoneNumber, vwPhoneNumber, 3)
                    isEnableButton()
                }
                R.id.etFullname -> {
                    statFullname = validationBackgroundView(etFullname, vwFullname, 3)
                    isEnableButton()
                }
                R.id.etEmail -> {
                    if (EmailValidator.isEmailValid(etEmail.text.toString())) {
                        statFullname = validationBackgroundView(etEmail, vwEmail, 0)
                        isEnableButton()
                    }
                }
                R.id.etPassword -> {
                    if (checkValidationPassword(etPassword, etRePassword)) {
                        statPassword = validationBackgroundView(etPassword, vwPassword, 6)
                        statRePassword = validationBackgroundView(etRePassword, vwRePassword, 6)
                        isEnableButton()
                    }
                }
                R.id.etRePassword -> {
                    if (checkValidationPassword(etPassword, etRePassword)) {
                        statPassword = validationBackgroundView(etPassword, vwPassword, 6)
                        statRePassword = validationBackgroundView(etRePassword, vwRePassword, 6)
                        isEnableButton()
                    }
                }
                R.id.etCodeReferal -> {
                    statCodeReferal = validationBackgroundView(etCodeReferal, vwCodeReferal, 6)
                    isEnableButton()
                }
            }
        }
    }

    @SuppressLint("ResourceType")
    private fun isEnableButton() {
        setStatValidation()
        if (statAllValidation) {
            btnLogin.isEnabled = true
            btnLogin.setBackgroundColor(R.color.colorMain)
        } else {
            btnLogin.isEnabled = false
            btnLogin.setBackgroundColor(R.color.colorDisableButton)
        }
    }

    private fun setStatValidation() {
        statAllValidation = statPhoneNumber && statFullname && statPassword && statRePassword && statCodeReferal
                && statAgreeTermsConditions && statAgreeConnectedPede
    }

    private fun checkValidationPassword(password: EditText?, rePassword: EditText?): Boolean {
        return password!!.text.toString() == rePassword!!.text.toString()
    }

    @SuppressLint("ResourceType")
    private fun validationBackgroundView(editText: EditText?, viewLine: View?, minChar: Int): Boolean {
        if (editText!!.text.length >= minChar) {
            viewLine!!.setBackgroundColor(Color.parseColor(getString(R.color.colorDefaultCursorEditText)))
            statusField = true
            return true
        } else {
            viewLine!!.setBackgroundColor(Color.parseColor(getString(R.color.colorNote)))
            statusField = false
            return false
        }
    }

}
