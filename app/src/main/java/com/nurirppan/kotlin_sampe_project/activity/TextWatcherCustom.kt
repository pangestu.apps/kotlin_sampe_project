package com.nurirppan.kotlin_sampe_project.activity

import android.text.Editable
import android.text.TextWatcher
import android.view.View


class TextWatcherCustom(
    internal var view: View,
    internal var interFaceAfterChange: AfterTextChange?) : TextWatcher {

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(s: Editable) {
        if (interFaceAfterChange != null) {
            interFaceAfterChange!!.afterTextChanged(s, view)
        }
    }

    interface AfterTextChange {
        fun afterTextChanged(s: Editable, v: View)
    }
}